//
//  SignInViewController.swift
//  SignIn
//
//  Created by  drake on 2020/04/26.
//  Copyright © 2020 drake. All rights reserved.
//

import UIKit

let loginURLString = "http://localhost:3000/loginUsers"

class SignInViewController: UIViewController {
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var buttonSignIn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonSignIn.layer.cornerRadius = buttonSignIn.bounds.size.height/2
        buttonSignIn.layer.borderWidth = 1
        buttonSignIn.layer.borderColor = UIColor.blue.cgColor
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func SignInAPICall(_ sender: Any) {
        // parameters
        // get post
        // URLSession
        
        let param = [
            "userName" : userNameTextField.text ?? "",
            "password" : passwordTextField.text ?? ""
        ]
        
        //network
        //let loginURL = URL(stirng: "http://localhost:3000/loginUsers" + "?" + param.queryString)
        var urlComponents = URLComponents(string: loginURLString)
        urlComponents?.query = param.queryString
        
        guard let hasURL = urlComponents?.url else {
            return
        }
        
        //model
        URLSession.shared.dataTask(with: hasURL) { (data, respon, error) in
            
            guard let data = data else {
                return
            }
            
            let decoder = JSONDecoder()
            
            do {
                let user = try decoder.decode([LoginUser].self, from: data)
                print("user => \(user)")
                
                if let hasUserInfo = user.first {
                    User.shared.info = hasUserInfo
        
                    NotificationCenter.default.post(name: NSNotification.Name.init("UserInfoLoad"), object: nil)
                    
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                    }
                } else{
                    DispatchQueue.main.async {
                        let alert = UIAlertController.init(title: "유저정보가 없음", message: nil, preferredStyle:.alert)
                        alert.addAction(UIAlertAction(title: "확인", style: .default, handler:
                            { (element) in
                            }
                        ) )
                        alert.addAction(UIAlertAction(title: "취소", style: .default, handler:
                            { (element) in
                            }
                        ) )
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            } catch {
                print("error ==> \(error)")
            }
        }.resume()
    }
    
    @IBAction func moveToSignUp(_ sender: Any) {
        let signUpVC = UIStoryboard(name: "SignUpVC", bundle: nil).instantiateViewController(identifier: "SignUpVC")
        
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    @IBAction func dismissVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
