//
//  LoginUser.swift
//  SignIn
//
//  Created by  drake on 2020/05/10.
//  Copyright © 2020 drake. All rights reserved.
//

import UIKit

class User {
    static let shared = User()
    var info = LoginUser()
    
    private init() {
        
    }
}

struct LoginUser: Codable {
    
    init() {
        name = ""
        password = ""
        email = ""
        id = 0
        location = [LocationModel]()
    }
    
    struct LocationModel: Codable {
        let city: String
        let state: String
    }
    
    let name: String?
    let password: String?
    let email: String?
    let id: Int?
    let location: [LocationModel]?
}
