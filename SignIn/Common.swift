//
//  Common.swift
//  SignIn
//
//  Created by  drake on 2020/05/10.
//  Copyright © 2020 drake. All rights reserved.
//

import UIKit

extension Dictionary {
    var queryString: String {
        var output = ""
        for (key, value) in self {
            output = output + "\(key)=\(value)&"
        }
        
        output = String(output.dropLast())
        return output
    }
}
