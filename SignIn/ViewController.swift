//
//  ViewController.swift
//  SignIn
//
//  Created by  drake on 2020/04/20.
//  Copyright © 2020 drake. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var userInfoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(infoReLoad), name: NSNotification.Name.init("UserInfoLoad"), object: nil)
    }
    
    @objc func infoReLoad() {
        DispatchQueue.main.async {
            self.userInfoLabel.text = User.shared.info.email
        }
    }

    @IBAction func moveToSignIn(_ sender: Any) {
        let SignInVC = UIStoryboard(name: "SignInVC", bundle: nil).instantiateViewController(identifier: "NaviCSignin")
        
        self.present(SignInVC, animated: true, completion: nil)
    }
    
}

