//
//  SignUpViewController.swift
//  SignIn
//
//  Created by  drake on 2020/05/05.
//  Copyright © 2020 drake. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var buttonSignUp: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonSignUp.layer.cornerRadius = buttonSignUp.bounds.size.height/2
        buttonSignUp.layer.borderWidth = 1
        buttonSignUp.layer.borderColor = UIColor.blue.cgColor
    }
    
    @IBAction func SignUpApiCall(_ sender: Any) {
        let params = [
            "name" : nameField.text ?? "",
            "password" : passwordField.text ?? "",
            "email" : emailField.text ?? ""
        ]
        
        //HTTTP Method -> POST
        //query => body
        
        //GET => URL + query
        if let url 	= URL(string: "http://localhost:3000/loginUsers") {
            var request = URLRequest(url: url)
            
            request.httpMethod = "POST"
            request.httpBody = params.queryString.data(using: .utf8)
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                    return
                }
                
                do {
                    let decoder = JSONDecoder()
                    let user = try decoder.decode(LoginUser.self, from: data)
                    
                    User.shared.info = user
                    
                    NotificationCenter.default.post(name: NSNotification.Name.init("UserInfoLoad"), object: nil)
                    
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                    }
                } catch {
                    print("Sign Up Error \(error)")
                }
            }.resume()
        }
    }
    
    @IBAction func popVC(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
